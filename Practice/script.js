// 1. Попросіть користувача ввести свій вік за допомогою prompt.
// Якщо вік менше 12 років, виведіть у вікні alert повідомлення про те,
// що він є дитиною, якщо вік менше 18 років, виведіть повідомлення про те, 
// що він є підлітком, інакше виведіть повідомлення про те, що він є дорослим.

let ageInput = prompt("Введіть ваш вік:");

if (isNaN(ageInput)) {
  alert("Введено не число!");
} else {
  let age = parseInt(ageInput);

  if (age < 12) {
    alert("Ви є дитиною.");
  } else if (age >= 12 && age < 18) {
    alert("Ви є підлітком.");
  } else {
    alert("Ви є дорослим.");
  }
}



// 2. Напишіть програму, яка запитує у користувача місяць року (українською мовою маленкими літерами) 
// та виводить повідомлення, скільки днів у цьому місяці. Результат виводиться в консоль. Скільки днів 
// в кожному місяці можна знайти тут в розділі Юліанський і Григоріанський календарі - 
// https://uk.wikipedia.org/wiki/%D0%9C%D1%96%D1%81%D1%8F%D1%86%D1%8C_(%D1%87%D0%B0%D1%81). (Використайте switch case)

const monthName = prompt("Введіть назву місяця (маленькими літерами): ");

const date = new Date(2024, getMonthNumber(monthName), 1);
const daysInMonth = new Date(date.getFullYear(), date.getMonth() + 1, 0).getDate();

console.log(`У ${monthName} ${daysInMonth} днів.`);
alert(`У ${monthName} ${daysInMonth} днів.`);

function getMonthNumber(monthName) {
  switch (monthName) {
    case "січень":
      return 0;
    case "лютий":
      return 1;
    case "березень":
      return 2;
    case "квітень":
      return 3;
    case "травень":
      return 4;
    case "червень":
      return 5;
    case "липень":
      return 6;
    case "серпень":
      return 7;
    case "вересень":
      return 8;
    case "жовтень":
      return 9;
    case "листопад":
      return 10;
    case "грудень":
      return 11;
    default:
      return -1;
  }
}
